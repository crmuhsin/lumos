module.exports = {
  themeConfig: {
    title: 'LUMOS',
    head: [
      ['link', { rel: 'icon', href: `/favicon.png` }]
    ],
    logo: '/logo.png',
    sidebar: {

      '/guide/': sidebarGuide()
    }
  }
}

function sidebarGuide() {
  return [
    {
      collapsable: false,
      children: ['']
    },
    {
      title: 'Meta',
      children: [
        'asset-category',
        'asset-subcategory'
      ]
    },
    {
      title: 'Master',
      children: [
        'manufacturer',
        'asset-model',
        'vendor'
      ]
    },
    {
      title: 'Operation',
      children: [
        'organization',
        'customer',
        'site',
        'asset-owner',
        'asset',
        'asset-issue-log',
        'requisition'
      ]
    },
    {
      title: 'Settings',
      children: [
        'role',
        'user',
        'account-settings',
        'signout'
      ]
    }
  ]
}