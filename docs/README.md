---
home: true
heroImage: logo.png
actionText: Documentation
actionLink: /guide/
navbar: false
features:
- title: Features
  details: Managing Different types of Product is easier.
- title: What is LUMOS?
  details: An Asset Management Tool for Organizations.
- title: About this DOC
  details: This Documentation has made to make life easier for LUMOS users.
footer: Developed using VuePress by LUMOS team
---
