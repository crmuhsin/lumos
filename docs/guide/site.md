# Site

## Site List

Clicking the Site, Site list will be visible. 
![Site List Blank](../.vuepress/public/asset/site/01siteblanklist.png)

## Add Site
Click the ‘+New’ button from list for adding a new Site

![Site New Button](../.vuepress/public/asset/site/02sitenewbutton.png)

In the form, input Site name and click the plus icon for adding the basic specification. Save button will be enabled after filling all required field. By clicking ‘Cancel’ button user will redirect to Site list.

![Site Add Page](../.vuepress/public/asset/site/03siteaddpage.png)


## Sort Site

The list can be sorted by clicking on ‘Site’ title of the table and arrow sign will indicate currently sorting alphabetical order ( ascending or descending ).

![Site List Sort](../.vuepress/public/asset/site/04sitelistsort.png)

## Filter Site
For filtering the data just type in the search box and related data will be shown

![Site Filter](../.vuepress/public/asset/site/05sitelistfilter.png)



## Update Site

For edit the existing Site, click the ‘edit icon’ from the particular row of the table.

![Site Edit](../.vuepress/public/asset/site/06siteedit.png)


Previous data will be set on the form and user can edit the data from there. After finish editing, clicking the ‘Update’ button will update the data. For going back to list click the cancel button.

![Site Update Page](../.vuepress/public/asset/site/07siteupdatepage.png)

## Change Status of Site

Site status can be changed by clicking the white toggle button from active to inactive and inactive to active. When active, button status will be green and when inactive, button status will be red.

![Site Inactive](../.vuepress/public/asset/site/08sitedelin.png)


![Site Active](../.vuepress/public/asset/site/09sitedelac.png)