# Asset Model

## Asset Model List

Clicking the asset Model, Model list will be visible. 
![Model List Blank](../.vuepress/public/asset/model/01modelblanklist.png)

## Add Asset Model
Click the ‘+New’ button from list for adding a new Model

![Model New Button](../.vuepress/public/asset/model/02modelnewbutton.png)

In the form, input Model name and click the plus icon for adding the basic specification. Save button will be enabled after filling all required field. By clicking ‘Cancel’ button user will redirect to Model list.

![Model Add Page](../.vuepress/public/asset/model/03modeladdpage.png)


## Sort Asset Model

The list can be sorted by clicking on ‘Model’ title of the table and arrow sign will indicate currently sorting alphabetical order ( ascending or descending ).

![Model List Sort](../.vuepress/public/asset/model/04modellistsort.png)

## Filter Asset Model
For filtering the data just type in the search box and related data will be shown

![Model Filter](../.vuepress/public/asset/model/05modellistfilter.png)



## Update Asset Model

For edit the existing Model, click the ‘edit icon’ from the particular row of the table.

![Model Edit](../.vuepress/public/asset/model/06modeledit.png)


Previous data will be set on the form and user can edit the data from there. After finish editing, clicking the ‘Update’ button will update the data. For going back to list click the cancel button.

![Model Update Page](../.vuepress/public/asset/model/07modelupdatepage.png)

## Delete Asset Model

If the Model is not assigned to any asset then the Model can be delete by clicking the delete button of a particular row from the list.

![Model Delete](../.vuepress/public/asset/model/08modeldelete.png)

Click 'Yes' to confirm deletion.

![Model Delete Confirm](../.vuepress/public/asset/model/09modeldel.png)