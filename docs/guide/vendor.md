# Vendor

## Vendor List

Clicking the Vendor, Vendor list will be visible. 
![Vendor List Blank](../.vuepress/public/asset/vendor/01vendblanklist.png)

## Add Vendor
Click the ‘+New’ button from list for adding a new Vendor

![Vendor New Button](../.vuepress/public/asset/vendor/02vendnewbutton.png)

In the form, input Vendor name and click the plus icon for adding the basic specification. Save button will be enabled after filling all required field. By clicking ‘Cancel’ button user will redirect to Vendor list.

![Vendor Add Page](../.vuepress/public/asset/vendor/03vendaddpage.png)


## Sort Vendor

The list can be sorted by clicking on ‘Vendor’ title of the table and arrow sign will indicate currently sorting alphabetical order ( ascending or descending ).

![Vendor List Sort](../.vuepress/public/asset/vendor/04vendlistsort.png)

## Filter Vendor
For filtering the data just type in the search box and related data will be shown

![Vendor Filter](../.vuepress/public/asset/vendor/05vendlistfilter.png)



## Update Vendor

For edit the existing Vendor, click the ‘edit icon’ from the particular row of the table.

![Vendor Edit](../.vuepress/public/asset/vendor/06vendedit.png)


Previous data will be set on the form and user can edit the data from there. After finish editing, clicking the ‘Update’ button will update the data. For going back to list click the cancel button.

![Vendor Update Page](../.vuepress/public/asset/vendor/07vendupdatepage.png)

## Delete Vendor

If the Vendor is not assigned to any asset then the Vendor can be delete by clicking the delete button of a particular row from the list.

![Vendor Delete](../.vuepress/public/asset/vendor/08venddelete.png)

Click 'Yes' to confirm deletion.

![Vendor Delete Confirm](../.vuepress/public/asset/vendor/09venddel.png)