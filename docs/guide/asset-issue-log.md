# Asset Issue Log

## Asset Issue

Click the issue button of a particular asset to see it’s issues

![Issue](../.vuepress/public/asset/issue/01.png)

## Create Issue
Click the ‘+New’ button from here for creating a new issue

![Issue](../.vuepress/public/asset/issue/02.png)

Input values in the respective fields to create a issue.

![Issue](../.vuepress/public/asset/issue/03.png)

## Filtering Issue Log

Clicking the ‘issue’ button from asset list will show the available issues of a asset. The list can be sorted by clicking on titles of the table and arrow sign will indicate currently sorting alphabetical order ( ascending or descending ).

![Issue](../.vuepress/public/asset/issue/04.png)

## Update Issue

For edit the existing issue, click the ‘edit icon’ from the particular row of the table.

![Issue](../.vuepress/public/asset/issue/05.png)

Previous data will be set on the form and user can edit the data from there. After finish editing, clicking the ‘Update’ button will update the data. For going back to list click the cancel button.

![Issue](../.vuepress/public/asset/issue/06.png)

## Resolving Issue

To resolve a issue change the status to resolve and new input fields will be visible for taking necessary information

![Issue](../.vuepress/public/asset/issue/07.png)

## Issue Details and Comments

For see the detail of a issue and commenting click the comment button.

![Issue](../.vuepress/public/asset/issue/08.png)

First panel shows the currently available details of a issue. Comment against a issue can be added from here.

![Issue](../.vuepress/public/asset/issue/09.png)
