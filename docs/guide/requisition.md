# Requisition

## Requisition List

Clicking the Requisition, requisition list will be visible. 

![Requisition](../.vuepress/public/asset/requisition/01.png)

## Add Requisition

Click the ‘+New’ button from list for adding a new requisition.

![Requisition](../.vuepress/public/asset/requisition/02.png)


Input value in fields to create a requisition. By clicking ‘Cancel’ button user will redirect to requisition list.

![Requisition](../.vuepress/public/asset/requisition/03.png)

## Sort Requisition List

The list can be sorted by clicking on titles of the table and arrow sign will indicate currently sorting alphabetical order ( ascending or descending ).

![Requisition](../.vuepress/public/asset/requisition/04.png)

## Filtering Requisition List

For filtering the data just type in the search box and related data will be shown

![Requisition](../.vuepress/public/asset/requisition/05.png)

## Update Requisition

For edit the existing requisition click the ‘edit icon’ from the particular row of the table.

![Requisition](../.vuepress/public/asset/requisition/06.png)

Previous data will be set on the form and user can edit the data from there. After finish editing, clicking the ‘Update’ button will update the data. For going back to list click the cancel button.

![Requisition](../.vuepress/public/asset/requisition/07.png)

## Requisition Details

To see the requisition details , click the details button from the list.

![Requisition](../.vuepress/public/asset/requisition/08.png)

To see the available asset for the specific category click ‘see available asset’

![Requisition](../.vuepress/public/asset/requisition/09.png)

To see the proposed asset for the specific category click ‘see proposed asset’.

![Requisition](../.vuepress/public/asset/requisition/10.png)

To see the approved asset for the specific category click ‘see approved asset’.

![Requisition](../.vuepress/public/asset/requisition/11.png)
