# Account Settings

## Change Password

For changing password click on the username.

![Change Pass](../.vuepress/public/asset/dash/01chp.png)

Input the old password and new password to change password 

![Change Pass](../.vuepress/public/asset/dash/02chp.png)
