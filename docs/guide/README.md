# Dashboard
To go to the dashboard, We have to first [Sign in](/guide/#sign-in).

## Introduction

After login, user will land to Dashboard and will see number of asset for available category.

![Dash View Image](../.vuepress/public/asset/dash/02dash.png)


This is an asset management software. At last we will **manage** some asset here. Manage means we can **create** new asset, **update** them with additional information and if not needed, we can also **delete** them.

---

Now, to add new asset we have to follow this flow


* From **Meta** menu
  * create a new __Asset Category__ 
  * create a new __Asset Subcategory__
* From **Master** menu
  * create a new __Manufacturer__
  * create a new __Asset Model__
  * create a new __Vendor__
* From **Operation** menu
  * create a new __Customer__
  * create a new __Site__
---

After Creating an Asset we can then assign that to an __Asset Owner__


## Sign In
Input Username and password and then press Sign In

![Sign In Image](../.vuepress/public/asset/dash/01signin.png)

