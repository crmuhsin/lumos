# Asset Subcategory

## Asset Subcategory List

Clicking the asset Subcategory, Subcategory list will be visible. 
![Subcategory List Blank](../.vuepress/public/asset/subcategory/01subcatblanklist.png)

## Add Asset Subcategory
Click the ‘+New’ button from list for adding a new Subcategory

![Subcategory New Button](../.vuepress/public/asset/subcategory/02subcatnewbutton.png)

In the form, input Subcategory name and click the plus icon for adding the basic specification. Save button will be enabled after filling all required field. By clicking ‘Cancel’ button user will redirect to Subcategory list.

![Subcategory Add Page](../.vuepress/public/asset/subcategory/03subcataddpage.png)


## Sort Asset Subcategory

The list can be sorted by clicking on ‘Subcategory’ title of the table and arrow sign will indicate currently sorting alphabetical order ( ascending or descending ).

![Subcategory List Sort](../.vuepress/public/asset/subcategory/04subcatlistsort.png)

## Filter Asset Subcategory
For filtering the data just type in the search box and related data will be shown

![Subcategory Filter](../.vuepress/public/asset/subcategory/05subcatlistfilter.png)



## Update Asset Subcategory

For edit the existing Subcategory, click the ‘edit icon’ from the particular row of the table.

![Subcategory Edit](../.vuepress/public/asset/subcategory/06subcatedit.png)


Previous data will be set on the form and user can edit the data from there. After finish editing, clicking the ‘Update’ button will update the data. For going back to list click the cancel button.

![Subcategory Update Page](../.vuepress/public/asset/subcategory/07subcatupdatepage.png)

## Delete Asset Subcategory

If the Subcategory is not assigned to any asset then the Subcategory can be delete by clicking the delete button of a particular row from the list.

![Subcategory Delete](../.vuepress/public/asset/subcategory/08subcatdelete.png)

Click 'Yes' to confirm deletion.

![Subcategory Delete Confirm](../.vuepress/public/asset/subcategory/09subcatdel.png)