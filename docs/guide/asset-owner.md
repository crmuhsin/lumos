# Asset Owner

## Asset Owner List

Clicking the asset Owner, Owner list will be visible. 
![Owner List Blank](../.vuepress/public/asset/owner/01ownerblanklist.png)

## Add Asset Owner
Click the ‘+New’ button from list for adding a new Owner

![Owner New Button](../.vuepress/public/asset/owner/02ownernewbutton.png)

In the form, input Owner name and click the plus icon for adding the basic specification. Save button will be enabled after filling all required field. By clicking ‘Cancel’ button user will redirect to Owner list.

![Owner Add Page](../.vuepress/public/asset/owner/03owneraddpage.png)


## Sort Asset Owner

The list can be sorted by clicking on ‘Owner’ title of the table and arrow sign will indicate currently sorting alphabetical order ( ascending or descending ).

![Owner List Sort](../.vuepress/public/asset/owner/04ownerlistsort.png)

## Filter Asset Owner
For filtering the data just type in the search box and related data will be shown

![Owner Filter](../.vuepress/public/asset/owner/05ownerlistfilter.png)



## Update Asset Owner

For edit the existing Owner, click the ‘edit icon’ from the particular row of the table.

![Owner Edit](../.vuepress/public/asset/owner/06owneredit.png)


Previous data will be set on the form and user can edit the data from there. After finish editing, clicking the ‘Update’ button will update the data. For going back to list click the cancel button.

![Owner Update Page](../.vuepress/public/asset/owner/07ownerupdatepage.png)