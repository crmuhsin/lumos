# Asset

## Asset List 
Clicking the ‘Asset’, asset list will be visible.

![Asset](../.vuepress/public/asset/asset/01.png)

## Add Asset

Click the ‘+New’ button from here for adding a new asset

![Asset](../.vuepress/public/asset/asset/02.png)

Input values in the respective fields. After filling all necessary field the ‘Save’ button will be enabled.

![Asset](../.vuepress/public/asset/asset/03.png)

Specification will be visible after selecting category. More specification can be added through ‘Add Specification’. 

And By Clicking Add new model, new model can be created from this page.

![Asset](../.vuepress/public/asset/asset/04.png)
![Asset](../.vuepress/public/asset/asset/04_1.png)

To input configuration, enter configuration name and value.

![Asset](../.vuepress/public/asset/asset/05.png)

Input credential type, username and password

![Asset](../.vuepress/public/asset/asset/06.png)

Enter document name and file or url.

![Asset](../.vuepress/public/asset/asset/07.png)

Input the required field. Now Save Button will be enabled. Click it for save Asset

By Clicking Add new owner, new owner can be created from this page.

![Asset](../.vuepress/public/asset/asset/08.png)
![Asset](../.vuepress/public/asset/asset/08_1.png)

## Sort Asset list

The list can be sorted by clicking any title of the table and arrow sign will indicate currently sorting alphabetical order ( ascending or descending ).

![Asset](../.vuepress/public/asset/asset/09.png)

## Filter Asset List

For searching  a particular asset upon a specific parameter input the keyword in the search box.

![Asset](../.vuepress/public/asset/asset/10.png)

Another way of filtering asset list is to click the filter button to get the filtering panel.  

![Asset](../.vuepress/public/asset/asset/11.png)

For filtering, click the checkbox and input the value.  

![Asset](../.vuepress/public/asset/asset/12.png)

## Update Asset

For edit the existing asset data, click the ‘edit icon’ from the particular row of the table.

![Asset](../.vuepress/public/asset/asset/13.png)

Previous data will be set on the form and user can edit the data from there. After finish editing, clicking the ‘Update’ button will update the data. For going back to list click the cancel button.

![Asset](../.vuepress/public/asset/asset/14.png)

## Clone Asset

For clone an asset data, click the ‘clone icon’ from the particular row of the table.

![Asset](../.vuepress/public/asset/asset/15.png)

Previous data will be set on the form and user can edit the data to create a new asset. The ‘Name’, ‘Serial’, ‘Asset ID’ field should be unique. 

![Asset](../.vuepress/public/asset/asset/16.png)

## Asset Profile

Click the profile button of a particular asset to see it’s profile

![Asset](../.vuepress/public/asset/asset/17.png)

![Asset](../.vuepress/public/asset/asset/18.png)

