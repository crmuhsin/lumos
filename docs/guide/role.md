# Role

## Role List

Clicking the Role, role list will be visible.

![Role](../.vuepress/public/asset/role/01.png)


## Add Role

Click the ‘+New’ button from list for adding a new role

![Role](../.vuepress/public/asset/role/02.png)

Input the values and choose menu for the role.

![Role](../.vuepress/public/asset/role/03.png)

## Update Role

For edit the existing role click the ‘edit icon’ from the particular row of the table.

![Role](../.vuepress/public/asset/role/04.png)

Previous data will be set on the form and user can edit the data from there. After finish editing, clicking the ‘Update’ button will update the data. For going back to list click the cancel button.

![Role](../.vuepress/public/asset/role/05.png)
