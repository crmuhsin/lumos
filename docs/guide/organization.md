# Organization

## Organization List

Clicking the Organization, Organization list will be visible. 
![Organization List Blank](../.vuepress/public/asset/organization/01orgblanklist.png)

## Add Organization
Click the ‘+New’ button from list for adding a new Organization

![Organization New Button](../.vuepress/public/asset/organization/02orgnewbutton.png)

In the form, input Organization name and click the plus icon for adding the basic specification. Save button will be enabled after filling all required field. By clicking ‘Cancel’ button user will redirect to Organization list.

![Organization Add Page](../.vuepress/public/asset/organization/03orgaddpage.png)


## Sort Organization

The list can be sorted by clicking on ‘Organization’ title of the table and arrow sign will indicate currently sorting alphabetical order ( ascending or descending ).

![Organization List Sort](../.vuepress/public/asset/organization/04orglistsort.png)

## Filter Organization
For filtering the data just type in the search box and related data will be shown

![Organization Filter](../.vuepress/public/asset/organization/05orglistfilter.png)



## Update Organization

For edit the existing Organization, click the ‘edit icon’ from the particular row of the table.

![Organization Edit](../.vuepress/public/asset/organization/06orgedit.png)


Previous data will be set on the form and user can edit the data from there. After finish editing, clicking the ‘Update’ button will update the data. For going back to list click the cancel button.

![Organization Update Page](../.vuepress/public/asset/organization/07orgupdatepage.png)

## Change Status of Organization

Organization status can be changed by clicking the white toggle button from active to inactive and inactive to active. When active, button status will be green and when inactive, button status will be red.

![Organization Inactive](../.vuepress/public/asset/organization/08orgdelin.png)


![Organization Active](../.vuepress/public/asset/organization/09orgdelac.png)