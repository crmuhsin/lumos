# Customer

## Customer List

Clicking the Customer, Customer list will be visible. 
![Customer List Blank](../.vuepress/public/asset/customer/01custblanklist.png)

## Add Customer
Click the ‘+New’ button from list for adding a new Customer

![Customer New Button](../.vuepress/public/asset/customer/02custnewbutton.png)

In the form, input Customer name and click the plus icon for adding the basic specification. Save button will be enabled after filling all required field. By clicking ‘Cancel’ button user will redirect to Customer list.

![Customer Add Page](../.vuepress/public/asset/customer/03custaddpage.png)


## Sort Customer

The list can be sorted by clicking on ‘Customer’ title of the table and arrow sign will indicate currently sorting alphabetical order ( ascending or descending ).

![Customer List Sort](../.vuepress/public/asset/customer/04custlistsort.png)

## Filter Customer
For filtering the data just type in the search box and related data will be shown

![Customer Filter](../.vuepress/public/asset/customer/05custlistfilter.png)



## Update Customer

For edit the existing Customer, click the ‘edit icon’ from the particular row of the table.

![Customer Edit](../.vuepress/public/asset/customer/06custedit.png)


Previous data will be set on the form and user can edit the data from there. After finish editing, clicking the ‘Update’ button will update the data. For going back to list click the cancel button.

![Customer Update Page](../.vuepress/public/asset/customer/07custupdatepage.png)

## Change Status of Customer

Customer status can be changed by clicking the white toggle button from active to inactive and inactive to active. When active, button status will be green and when inactive, button status will be red.

![Customer Inactive](../.vuepress/public/asset/customer/08custdelin.png)


![Customer Active](../.vuepress/public/asset/customer/09custdelac.png)