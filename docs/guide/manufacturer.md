# Manufacturer

## Manufacturer List

Clicking the Manufacturer, Manufacturer list will be visible. 
![Manufacturer List Blank](../.vuepress/public/asset/manufacturer/01manblanklist.png)

## Add Manufacturer
Click the ‘+New’ button from list for adding a new Manufacturer

![Manufacturer New Button](../.vuepress/public/asset/manufacturer/02mannewbutton.png)

In the form, input Manufacturer name and click the plus icon for adding the basic specification. Save button will be enabled after filling all required field. By clicking ‘Cancel’ button user will redirect to Manufacturer list.

![Manufacturer Add Page](../.vuepress/public/asset/manufacturer/03manaddpage.png)


## Sort Manufacturer

The list can be sorted by clicking on ‘Manufacturer’ title of the table and arrow sign will indicate currently sorting alphabetical order ( ascending or descending ).

![Manufacturer List Sort](../.vuepress/public/asset/manufacturer/04manlistsort.png)

## Filter Manufacturer
For filtering the data just type in the search box and related data will be shown

![Manufacturer Filter](../.vuepress/public/asset/manufacturer/05manlistfilter.png)



## Update Manufacturer

For edit the existing Manufacturer, click the ‘edit icon’ from the particular row of the table.

![Manufacturer Edit](../.vuepress/public/asset/manufacturer/06manedit.png)


Previous data will be set on the form and user can edit the data from there. After finish editing, clicking the ‘Update’ button will update the data. For going back to list click the cancel button.

![Manufacturer Update Page](../.vuepress/public/asset/manufacturer/07manupdatepage.png)

## Delete Manufacturer

If the Manufacturer is not assigned to any asset then the Manufacturer can be delete by clicking the delete button of a particular row from the list.

![Manufacturer Delete](../.vuepress/public/asset/manufacturer/08mandelete.png)

Click 'Yes' to confirm deletion.

![Manufacturer Delete Confirm](../.vuepress/public/asset/manufacturer/09mandel.png)