# User

## User List

Clicking the User, user list will be visible.

![User](../.vuepress/public/asset/user/01.png)


## Add User

Click ‘+New’ button from list for adding a new user

![User](../.vuepress/public/asset/user/02.png)

Input the values in the form. And choose role to select menu.

![User](../.vuepress/public/asset/user/03.png)

Select menu item and give access to customer and site.

![User](../.vuepress/public/asset/user/04.png)

## User List Settings

From the user list, searching, changing status of user and Resetting User password can be possible.

![User](../.vuepress/public/asset/user/05.png)

## Update User

For edit the existing role click the ‘edit icon’ from the particular row of the table.

![User](../.vuepress/public/asset/user/06.png)

Previous data will be set on the form and user can edit the data from there. After finish editing, clicking the ‘Update’ button will update the data. For going back to list click the cancel button.

![User](../.vuepress/public/asset/user/07.png)
