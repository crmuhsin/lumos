# Asset Category

## Asset Category List

Clicking the asset category, category list will be visible. 
![Category List Blank](../.vuepress/public/asset/category/01catblanklist.png)

## Add Asset Category
Click the ‘+New’ button from list for adding a new category

![Category New Button](../.vuepress/public/asset/category/02catnewbutton.png)

In the form, input category name and click the plus icon for adding the basic specification. Save button will be enabled after filling all required field. By clicking ‘Cancel’ button user will redirect to category list.

![Category Add Page](../.vuepress/public/asset/category/03cataddpage.png)


## Sort Asset Category

The list can be sorted by clicking on ‘Category’ title of the table and arrow sign will indicate currently sorting alphabetical order ( ascending or descending ).

![Category List Sort](../.vuepress/public/asset/category/04catlistsort.png)

## Filter Asset Category
For filtering the data just type in the search box and related data will be shown

![Category Filter](../.vuepress/public/asset/category/05catlistfilter.png)



## Update Asset Category

For edit the existing category, click the ‘edit icon’ from the particular row of the table.

![Category Edit](../.vuepress/public/asset/category/06catedit.png)


Previous data will be set on the form and user can edit the data from there. After finish editing, clicking the ‘Update’ button will update the data. For going back to list click the cancel button.

![Category Update Page](../.vuepress/public/asset/category/07catupdatepage.png)

## Delete Asset Category

If the category is not assigned to any asset then the category can be delete by clicking the delete button of a particular row from the list.

![Category Delete](../.vuepress/public/asset/category/08catdelete.png)

Click 'Yes' to confirm deletion.

![Category Delete Confirm](../.vuepress/public/asset/category/09catdel.png)